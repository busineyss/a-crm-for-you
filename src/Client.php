<?php

/*
 * Acrmforyou Client Request
 */

namespace Acrmforyou;

class Client {

    protected $_domain = "https://api.acrmforyou.com/fr/";
    protected $_url;
    protected $_curl_url;
    protected $_method;

    public function __construct($method = 'GET') {
        $this->_method = $method;
    }

    public function setToken($token) {
        $this->_bearer = $token->access_token;
    }

    public function sendRequest($params = array()) {

        // set headers
        $header = array();
        $header[] = 'Accept: application/json';
        $header[] = 'Content-Type: application/json';
        $header[] = 'Authorization: ' . $this->_bearer;

        $ch = curl_init($this->_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($this->_method == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        }

        $result = curl_exec($ch);
        curl_close($ch);

        // return response
        return json_decode($result);
    }

}
