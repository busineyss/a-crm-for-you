<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Company extends Client {

    protected $_path = 'company';
    protected $_crud_path = 'crud/company';

    public function search($keyword = null) {
        $this->_url = $this->_domain . $this->_path . '/list';
        if (!empty($keyword)) {
            $this->_url .= '/keyword/' . urlencode($keyword);
        }
        return $this->sendRequest();
    }

    public function searchByName($name) {
        $this->_url = $this->_domain . $this->_path . DS . 'list' . DS . 'keyword' . DS . urlencode($name);
        return $this->sendRequest();
    }

    public function create($params) {
        $this->_method = 'POST';
        $this->_url = $this->_domain . $this->_path . '/create';
        return $this->sendRequest($params);
    }

}
