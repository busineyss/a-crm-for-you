<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Contact extends Client {

    protected $_path = 'contact';
    
    public function search($keyword = null) {
        $this->_url = $this->_domain . $this->_path . '/list';
        if (!empty($keyword)) {
            $this->_url .= '/keyword/' . urlencode($keyword);
        }
        return $this->sendRequest();
    }

    public function searchByLastname($name) {
        $this->_url = $this->_domain . $this->_crud_path . '?filter[0][field]=lastname&filter[0][eq]==&filter[0][value]=' . urlencode($name);
        return $this->sendRequest();
    }

    public function create($params) {
        $this->_method = 'POST';
        $this->_url = $this->_domain . $this->_path . '/create';
        return $this->sendRequest($params);
    }
    
    public function update($params) {
        $this->_method = 'POST';
        $this->_url = $this->_domain . $this->_path . '/update';
        return $this->sendRequest($params);
    }

}



    
