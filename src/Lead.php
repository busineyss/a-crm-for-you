<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Lead extends Client {

    protected $_path = 'lead';

    public function create($params) {
        $this->_method = 'POST';
        $this->_url = $this->_domain . $this->_path . '/create';
        return $this->sendRequest($params);
    }

    public function search($keyword = null) {
        $this->_url = $this->_domain . $this->_path . '/list';
        if (!empty($keyword)) {
            $this->_url .= '/keyword/' . urlencode($keyword);
        }
        return $this->sendRequest();
    }

    /**
     * Construct properties of lead 
     * @param array $dictionnary
     * @param array $params
     * @return stdClass
     */
    public function properties($dictionnary = array(), $params = array(), $toJson = false) {
        $properties = new \stdClass();
        $properties->formData = new \stdClass();
        $properties->additionals = new \stdClass();
        if (empty($dictionnary) || empty($params)) {
            return $properties;
        }

        foreach ($params as $key => $value) {
            $object = new \stdClass();
            $object->name = $key;
            $object->value = $value;
            if (array_key_exists($key, $dictionnary)) {
                // add to formData
                if (!empty($dictionnary[$key]['label'])) {
                    $object->label = $dictionnary[$key]['label'];
                }
                if (!empty($dictionnary[$key]['type'])) {
                    $object->type = $dictionnary[$key]['type'];
                }
                $properties->formData->{$key} = $object;
            } else {
                // add to additionals
                $properties->additionals->{$key} = $object;
            }
        }

        if ($toJson) {
            return json_encode($properties);
        }

        return $properties;
    }

}
