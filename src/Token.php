<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Token extends Client{

    protected $_path = 'auth/access-token';
    protected $_client_id;
    protected $_client_key;
    
    public function __construct($clientId, $clientKey) {
        $this->_url = $this->_domain . $this->_path; 
        $this->_client_id = $clientId;
        $this->_client_key = $clientKey;
    }

    public function getToken() {
        $params = array(
            'client_id' => $this->_client_id,
            'client_key' => $this->_client_key
        );
        $ch = curl_init($this->_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        if (!empty($response->error)) {
            return $response;
        }

        return $response->data;
    }

}
